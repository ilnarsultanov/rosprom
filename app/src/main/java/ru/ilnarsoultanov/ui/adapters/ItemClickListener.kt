package ru.ilnarsoultanov.ui.adapters

interface ItemClickListener<T> {
    //как раз говорили про generic типы, решил добавить так. В итоге сможем использовать этот интефейс с любыми типами

    fun onDelete(item: T)
    fun onClick(item: T)
}