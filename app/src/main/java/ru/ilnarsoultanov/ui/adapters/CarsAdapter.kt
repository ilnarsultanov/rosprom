package ru.ilnarsoultanov.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_car.view.*
import ru.ilnarsoultanov.R
import ru.ilnarsoultanov.model.car.CarViewModel

class CarsAdapter(var data: MutableList<CarViewModel>?, val itemClickListener: ItemClickListener<CarViewModel>): RecyclerView.Adapter<CarsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_car, parent, false), itemClickListener)
    }

    override fun getItemCount(): Int {
        data?.let {
            
            return data!!.size
        } ?: run{
            return 0
        }
    }

    fun deleteItem(item: CarViewModel){
        data?.remove(item)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data!!.get(position)
        holder.bind(item)
    }

    class ViewHolder (view: View, private val itemClickListener: ItemClickListener<CarViewModel>) : RecyclerView.ViewHolder(view) {
        val titleView = view.title
        val descriptionView = view.description
        val priceView = view.price
        val deleteButton = view.deleteButton

        fun bind(item: CarViewModel){
            titleView.text = item.nameTruck
            descriptionView.text = item.comment
            priceView.text = item.price.plus(" ").plus(itemView.context.getString(R.string.ruble).toLowerCase()) //по правильному, нужно валюту получать с сервера, но с сервера не идет инфа. С валютой информативнее из-за этого добавил.

            deleteButton.setOnClickListener { itemClickListener.onDelete(item)  }
            itemView.setOnClickListener { itemClickListener.onClick(item) }
        }
    }
}