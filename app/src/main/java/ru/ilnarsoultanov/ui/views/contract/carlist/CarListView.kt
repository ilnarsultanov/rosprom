package ru.ilnarsoultanov.ui.views.contract.carlist

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.ilnarsoultanov.model.car.CarViewModel

interface CarListView: MvpView {
    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun showLoadView()
    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun hideLoadView()
    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun showError(error: String)
    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun setCarList(data: List<CarViewModel>?)
    @StateStrategyType(value = OneExecutionStateStrategy::class)
    fun deleteCar(item: CarViewModel)
}