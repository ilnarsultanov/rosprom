package ru.ilnarsoultanov.ui.views.contract

import android.os.Bundle
import androidx.annotation.IdRes


interface Router {
    fun routeTo(@IdRes fragmentId: Int, bundle: Bundle?)
}