package ru.ilnarsoultanov.ui.views.contract.carlist

import ru.ilnarsoultanov.model.car.CarViewModel

interface CarListPresenter {
    fun loadCarData()
    fun refresh()
    fun deleteClicked(item: CarViewModel)
}