package ru.ilnarsoultanov.ui.views.contract.carlist

import ru.ilnarsoultanov.model.car.CarCreateModel
import ru.ilnarsoultanov.model.car.CarViewModel

interface CarEditPresenter {
    fun saveClicked()
    fun changeCarName(name: String)
    fun changeCarComment(comment: String)
    fun changeCarPrice(price: String)
}