package ru.ilnarsoultanov.ui.views.contract.caredit

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.ilnarsoultanov.model.car.CarCreateModel

interface CarEditView: MvpView {
    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun showLoadView()
    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun hideLoadView()
    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun showError(error: String)
    @StateStrategyType(value = OneExecutionStateStrategy::class)
    fun showSaveSuccessStatus()
    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun showCarInfo(data: CarCreateModel?)
}