package ru.ilnarsoultanov.ui.views.fragments


import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_car_list.*
import ru.ilnarsoultanov.R
import ru.ilnarsoultanov.model.car.CarEditModel
import ru.ilnarsoultanov.model.car.CarViewModel
import ru.ilnarsoultanov.model.repository.cars.DefaultCarsRepository
import ru.ilnarsoultanov.model.repository.network.RospromApi
import ru.ilnarsoultanov.ui.adapters.CarsAdapter
import ru.ilnarsoultanov.ui.adapters.ItemClickListener
import ru.ilnarsoultanov.ui.presenters.CarListPresenterImpl
import ru.ilnarsoultanov.ui.views.contract.Router
import ru.ilnarsoultanov.ui.views.contract.carlist.CarListPresenter
import ru.ilnarsoultanov.ui.views.contract.carlist.CarListView
import java.lang.ref.WeakReference

class CarListFragment : BaseFragment(), CarListView {
    @InjectPresenter
    internal lateinit var presenter : CarListPresenterImpl
    lateinit var router: WeakReference<Router>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_car_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        swipeRefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN)
        swipeRefresh.setOnRefreshListener {
            presenter.refresh()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        assert(activity is Router)//нам нужен роутер иначе экзепшен
        router = WeakReference(activity as Router)
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun showLoadView() {
        if (!swipeRefresh.isRefreshing)
            swipeRefresh.isRefreshing = true
    }

    override fun hideLoadView() {
        swipeRefresh.isRefreshing = false
    }

    override fun setCarList(data: List<CarViewModel>?) {
        data?.let {
            recyclerView?.adapter = CarsAdapter(it.toMutableList(), object : ItemClickListener<CarViewModel>{
                override fun onDelete(item: CarViewModel) {
                    presenter.deleteClicked(item)
                }

                override fun onClick(item: CarViewModel) {
                    val bundle = Bundle()
                    bundle.putSerializable("carData", CarEditModel(item.id, item.price, item.comment, item.nameTruck))
                    bundle.putBoolean("modeIsCreate", false)
                    router.get()?.routeTo(R.id.carCreateFragment, bundle)
                }
            })
        } ?: run {
            recyclerView.adapter = null
        }
    }

    override fun deleteCar(item: CarViewModel) {
        recyclerView.adapter?.let {
            if (it is CarsAdapter){
                it.deleteItem(item)
            }
        }

        view?.let { Toast.makeText(it.context, getString(R.string.carDeleted), Toast.LENGTH_LONG).show() }
    }

    override fun showError(error: String) {
        view?.let { Toast.makeText(it.context, error, Toast.LENGTH_LONG).show() }
    }

    override fun onResume() {
        super.onResume()

        if (arguments!!.getBoolean("isRefresh")){
            presenter.refresh()
            arguments!!.putBoolean("isRefresh", false)
        }
    }

    @ProvidePresenter
    fun provideCarViewPresenter():CarListPresenterImpl{
        return CarListPresenterImpl(DefaultCarsRepository(RospromApi.getInstance()))
    }
}
