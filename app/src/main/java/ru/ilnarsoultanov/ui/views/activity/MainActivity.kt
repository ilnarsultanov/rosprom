package ru.ilnarsoultanov.ui.views.activity

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import kotlinx.android.synthetic.main.activity_main.*
import ru.ilnarsoultanov.R
import ru.ilnarsoultanov.ui.views.contract.Router


class MainActivity : AppCompatActivity(), Router {
    private val navController : NavController by lazy {
        Navigation.findNavController(this, R.id.nav_host_fragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        NavigationUI.setupWithNavController(nav_view, navController);
    }

    override fun routeTo(@IdRes fragmentId: Int, bundle: Bundle?) {
        navController.navigate(fragmentId, bundle)
    }
}
