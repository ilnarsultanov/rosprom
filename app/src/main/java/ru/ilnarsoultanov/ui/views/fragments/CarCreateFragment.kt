package ru.ilnarsoultanov.ui.views.fragments


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding.view.RxView
import com.jakewharton.rxbinding.widget.RxTextView
import kotlinx.android.synthetic.main.fragment_car_create.*
import ru.ilnarsoultanov.R
import ru.ilnarsoultanov.model.car.CarCreateModel
import ru.ilnarsoultanov.model.car.CarEditModel
import ru.ilnarsoultanov.model.repository.network.RospromApi
import ru.ilnarsoultanov.ui.presenters.CarEditPresenterImpl
import ru.ilnarsoultanov.ui.views.contract.Router
import ru.ilnarsoultanov.ui.views.contract.caredit.CarEditView
import rx.android.schedulers.AndroidSchedulers
import java.lang.ref.WeakReference


class CarCreateFragment : BaseFragment(), CarEditView {
    @InjectPresenter
    lateinit var presenter: CarEditPresenterImpl
    lateinit var router: WeakReference<Router>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_car_create, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        assert(activity is Router)//нам нужен роутер иначе экзепшен
        router = WeakReference(activity as Router)
    }

    @ProvidePresenter
    fun providePresenter(): CarEditPresenterImpl{
        //arguments всегда != null т.к. в графе для него есть дефаулт поля..
        val isCreateMode = arguments!!.getBoolean("modeIsCreate")

        if (isCreateMode){
            val carData = CarCreateModel()
            return CarEditPresenterImpl(isCreateMode, carData, RospromApi.getInstance())
        } else {
            val editData = arguments!!.getSerializable("carData") as CarEditModel
            val carData = CarCreateModel(editData.price, editData.comment, editData.nameTruck)
            val carId = editData.id!! //т.к. он никак не может быть null

            return CarEditPresenterImpl(isCreateMode, carData, RospromApi.getInstance(), carId)
        }
    }

    override fun showCarInfo(data: CarCreateModel?) {
        carName.editText?.setText(data?.nameTruck)
        carComment.editText?.setText(data?.comment)
        carPrice.editText?.setText(data?.price)
    }

    override fun showLoadView() {
        if (!swipeRefreshe.isRefreshing)
            swipeRefreshe.isRefreshing = true
    }

    override fun hideLoadView() {
        if (swipeRefreshe.isRefreshing)
            swipeRefreshe.isRefreshing = false
    }

    override fun showError(error: String) {
        view?.let { Toast.makeText(it.context, error, Toast.LENGTH_LONG).show() }
    }

    override fun showSaveSuccessStatus() {
        val message = when(arguments!!.getBoolean("modeIsCreate")){
            true -> getString(R.string.createCarSuccess)
            false -> getString(R.string.carInfoEditSuccess)
        }
        view?.let { Toast.makeText(it.context, message, Toast.LENGTH_LONG).show() }

        val bundle = Bundle()
        bundle.putBoolean("isRefresh", true)
        router.get()?.routeTo(R.id.carListFragment, bundle)
    }

    private fun initView() {
        swipeRefreshe.isEnabled = false

        if (!arguments!!.getBoolean("modeIsCreate")){
            complete.setText(getString(R.string.save))
        }

        RxView.clicks(complete)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                context?.hideKeyboard(complete)
                presenter.saveClicked()
            }

        RxTextView.textChanges(carName.editText!!)
            .skip(1)
            .map(CharSequence::toString)
            .subscribe({presenter.changeCarName(it)})

        RxTextView.textChanges(carComment.editText!!)
            .skip(1)
            .map(CharSequence::toString)
            .subscribe({
                Log.d("myTest", it + " carComment test")
                presenter.changeCarComment(it)})

        RxTextView.textChanges(carPrice.editText!!)
            .skip(1)
            .map(CharSequence::toString)
            .subscribe({
                Log.d("myTest", it + " carPrice test")
                presenter.changeCarPrice(it)
            })
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
