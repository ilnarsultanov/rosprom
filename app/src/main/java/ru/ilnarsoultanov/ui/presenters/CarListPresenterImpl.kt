package ru.ilnarsoultanov.ui.presenters

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.ilnarsoultanov.model.car.CarResponse
import ru.ilnarsoultanov.model.car.CarViewModel
import ru.ilnarsoultanov.model.repository.cars.contract.CarsRepository
import ru.ilnarsoultanov.ui.views.contract.carlist.CarEditPresenter
import ru.ilnarsoultanov.ui.views.contract.carlist.CarListPresenter
import ru.ilnarsoultanov.ui.views.contract.carlist.CarListView

@InjectViewState
class CarListPresenterImpl(private val repository: CarsRepository): MvpPresenter<CarListView>(), CarListPresenter {
    private var loadingDisposable: Disposable? = null
    private var deleteDisposable: Disposable? = null


    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadCarData()
    }

    override fun loadCarData() {
        loadingDisposable?.dispose()

        loadingDisposable = repository.getCars()
            .map { if (!it.isNullOrEmpty()) it else throw Exception("Транспортные средства не найдены")  } //в продакшене можно было бы по разному обработать, например если нет контента то сервер возвращает статус 204. И в репозитории смотреть все эти статусы и во что то их мапить
            .flatMap { Observable.fromIterable(it) }
            .filter{ !it.id.isNullOrEmpty() && !it.nameTruck.isNullOrEmpty() && !it.comment.isNullOrEmpty() && priceIsValid(it.price)}
            .distinct { it.id } //пропускаем дубликаты, но такого не должно быть...
            .map { CarViewModel(it.price, it.comment, it.id!!, it.nameTruck!!) }
            .toList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally{viewState.hideLoadView()}
            .doOnSubscribe{
                viewState.setCarList(null)
                viewState.showLoadView()
            }
            .subscribe({
                viewState.setCarList(it)
            }, {
                val error = it.message?.let { it }?: run {it.toString()}
                viewState.showError(error)// в продакшене поменяли бы все это на какой нибудь errorHandler и не было бы такого колхоза
            })
    }

    override fun refresh() {
        loadCarData()
    }

    override fun deleteClicked(item: CarViewModel) {
        deleteDisposable?.dispose()

        deleteDisposable = repository.deleteCar(item.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally{viewState.hideLoadView()}
            .doOnSubscribe{
                viewState.showLoadView()
            }
            .subscribe({
                viewState.deleteCar(item)
            }, {
                val error = it.message?.let { it }?: run {it.toString()}
                viewState.showError(error)// в продакшене поменяли бы все это на какой нибудь errorHandler и не было бы такого колхоза
            })
    }

    override fun onDestroy() {
        super.onDestroy()

        deleteDisposable?.dispose()
        loadingDisposable?.dispose()
    }

    fun priceIsValid(price:String?) : Boolean{
        val pr = price?.replace(",", ".")?.toDoubleOrNull()
        pr?.let {
            return it >= 0
        } ?: run {
            return false
        }
    }
}