package ru.ilnarsoultanov.ui.presenters

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.ilnarsoultanov.model.car.CarCreateModel
import ru.ilnarsoultanov.model.repository.network.RospromApi
import ru.ilnarsoultanov.ui.views.contract.caredit.CarEditView
import ru.ilnarsoultanov.ui.views.contract.carlist.CarEditPresenter


@InjectViewState
class CarEditPresenterImpl(private var modeIsCreate: Boolean, private var data: CarCreateModel, private val api: RospromApi): MvpPresenter<CarEditView>(),  CarEditPresenter{
    var carId: String? = null

    constructor(modeIsCreate: Boolean, data: CarCreateModel, api: RospromApi, carId: String):
            this(modeIsCreate, data, api){
        this.carId = carId
    }

    var saveDisposable: Disposable? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.showCarInfo(data)
    }

    override fun onDestroy() {
        super.onDestroy()

        saveDisposable?.dispose()
    }

    override fun saveClicked() {
        saveDisposable?.dispose()

        if (data.comment.isNullOrEmpty() || data.price.isNullOrEmpty() || data.nameTruck.isNullOrEmpty()){
            viewState.showError("Заполните все поля")
            return
        }

        if (modeIsCreate) {
            createCar(data)
        } else {
            carId?.let { editCar(data) } ?: run {  viewState.showError("Не указан id автомобиля") }
        }
    }

    override fun changeCarName(name: String) {
        data.nameTruck = name
    }

    override fun changeCarComment(comment: String) {
        data.comment = comment
    }

    override fun changeCarPrice(price: String) {
        data.price = price
    }


    private fun createCar(car: CarCreateModel){
        saveDisposable = api.createCar(car)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally{viewState.hideLoadView()}
            .doOnSubscribe{
                viewState.showLoadView()
            }
            .subscribe({
                viewState.showSaveSuccessStatus()
            }, {
                viewState.showError(it.toString())
            })
    }

    private fun editCar(car: CarCreateModel){
        saveDisposable = api.editCar(carId!!, car)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally{viewState.hideLoadView()}
            .doOnSubscribe{
                viewState.showLoadView()
            }
            .subscribe({
                viewState.showSaveSuccessStatus()
            }, {
                viewState.showError(it.toString())
            })
    }

}