package ru.ilnarsoultanov.model.repository.network

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import ru.ilnarsoultanov.model.car.CarCreateModel
import ru.ilnarsoultanov.model.car.CarCreateResponse
import ru.ilnarsoultanov.model.car.CarResponse
import ru.ilnarsoultanov.model.repository.network.Constants.Constants.END_POINT

interface RospromApi {

    @GET("trucks")
    fun getCars(): Observable<List<CarResponse>>

    @HTTP(method = "DELETE", path = END_POINT.plus("truck/{id}"), hasBody = true)
    fun deleteCar(@Path("id") id: String): Observable<Int>

    @POST("truck/add")
    fun createCar(@Body body: CarCreateModel): Observable<CarCreateResponse>

    @PATCH("truck/{id}")
    fun editCar(@Path("id") id: String, @Body body: CarCreateModel): Observable<CarCreateResponse>

    companion object {
        fun getInstance(): RospromApi {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(END_POINT)
                .build()

            return retrofit.create(RospromApi::class.java)
        }
    }
}