package ru.ilnarsoultanov.model.repository.cars.contract

import io.reactivex.Observable
import ru.ilnarsoultanov.model.car.CarCreateModel
import ru.ilnarsoultanov.model.car.CarCreateResponse
import ru.ilnarsoultanov.model.car.CarResponse

interface CarsRepository {
    fun getCars():Observable<List<CarResponse>>
    fun deleteCar(id: String):Observable<Int>
    fun createCar(car: CarCreateModel):Observable<CarCreateResponse>
    fun editCar(car: CarCreateModel):Observable<CarCreateResponse>
}