package ru.ilnarsoultanov.model.repository.cars

import io.reactivex.Observable
import retrofit2.Response
import ru.ilnarsoultanov.model.car.CarCreateModel
import ru.ilnarsoultanov.model.car.CarCreateResponse
import ru.ilnarsoultanov.model.car.CarResponse
import ru.ilnarsoultanov.model.repository.cars.contract.CarsRepository
import ru.ilnarsoultanov.model.repository.network.RospromApi

class DefaultCarsRepository(val networkApi: RospromApi): CarsRepository {

    override fun getCars(): Observable<List<CarResponse>> {
        return networkApi.getCars()
    }

    override fun deleteCar(id: String): Observable<Int> {
        return networkApi.deleteCar(id)
    }

    override fun createCar(car: CarCreateModel): Observable<CarCreateResponse> {
        return networkApi.createCar(car)
    }

    override fun editCar(car: CarCreateModel): Observable<CarCreateResponse> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}