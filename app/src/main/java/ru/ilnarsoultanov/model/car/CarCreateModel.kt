package ru.ilnarsoultanov.model.car

import com.google.gson.annotations.SerializedName

data class CarCreateModel(

	@field:SerializedName("price")
	var price: String? = null,

	@field:SerializedName("comment")
	var comment: String? = null,

	@field:SerializedName("nameTruck")
	var nameTruck: String? = null
)