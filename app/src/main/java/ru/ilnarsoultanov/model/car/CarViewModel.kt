package ru.ilnarsoultanov.model.car

import java.io.Serializable

//как вариант, чтобы вью не зависила от модели ответа сервера
data class CarViewModel (
    val price: String? = null,
    val comment: String? = null,
    val id: String,
    val nameTruck: String
)