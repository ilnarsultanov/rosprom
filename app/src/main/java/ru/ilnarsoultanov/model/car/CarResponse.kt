package ru.ilnarsoultanov.model.car

import com.google.gson.annotations.SerializedName

data class CarResponse(
	@field:SerializedName("price")
	val price: String? = null,

	@field:SerializedName("comment")
	val comment: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("nameTruck")
	val nameTruck: String? = null
)