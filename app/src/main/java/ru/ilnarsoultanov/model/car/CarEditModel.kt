package ru.ilnarsoultanov.model.car

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CarEditModel(

	@field:SerializedName("id")
	var id: String? = null,

	@field:SerializedName("price")
	var price: String? = null,

	@field:SerializedName("comment")
	var comment: String? = null,

	@field:SerializedName("nameTruck")
	var nameTruck: String? = null
): Serializable